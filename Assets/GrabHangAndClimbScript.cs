﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrabHangAndClimbScript : MonoBehaviour {

    public HangingState hangingState;
    RaycasterScript raycasterScript;
    MovementControllerScript movementControllerScript;
    public float grabPointY;
    public float distanceAboveGrabPoint;
    int dropFrames = 0;
    int climbFrames = 0;
    public GameObject thingImHangingOn;
    public enum HangingState

    {
        HANGING,
        CLIMBING_UP,
        CLIMBING_DOWN,
        DROPPING,
        NONE
    }

    public void Start()
    {
        hangingState = HangingState.NONE;
        raycasterScript = GetComponent<RaycasterScript>();
        movementControllerScript = GetComponent<MovementControllerScript>();
    }

    public void MovementUpdate()
    {

        DetermineHangingState();
        if (thingImHangingOn != null)
        {
            if (thingImHangingOn.GetComponent<PassengerHandlerScript>())
            {
                thingImHangingOn.GetComponent<PassengerHandlerScript>().hangers.Add(gameObject);
            }
        }

        if (hangingState == HangingState.HANGING)
        {
            movementControllerScript.velocity.y = 0;
        }
        else if (hangingState == HangingState.CLIMBING_UP)
        {
            climbUp();
            movementControllerScript.isClimbing = true;
        }
        else if (hangingState == HangingState.CLIMBING_DOWN)
        {
            climbDown();
        }
    }

        public void climbDown()
    {

    }

    public void startClimbUp()
    {
        hangingState = HangingState.CLIMBING_UP;
    }

    public void TryToClimbDown()
    {
        Vector2 downVec = Vector2.down;
        raycasterScript.HorizontalCollisions(ref downVec);

        GameObject leftMostHit = raycasterScript.collisionState.bottomHits[0];
        GameObject rightMostHit = raycasterScript.collisionState.bottomHits[raycasterScript.collisionState.bottomHits.Length-1];

        int climbDownDirection = 0;

        if (leftMostHit == null && rightMostHit != null)
        {
            climbDownDirection = -1;
        }
        else if (leftMostHit != null && rightMostHit == null)
        {
            climbDownDirection = 1;
        }
        Debug.Log("try.");

        if (climbDownDirection != 0)
        {
            Debug.Log("succeed.");
            movementControllerScript.facing = climbDownDirection*-1;
            hangingState = HangingState.CLIMBING_DOWN;
        }
    }

    void climbUp()
    {
        climbFrames++;
        Vector2 facing = new Vector2(movementControllerScript.facing*.1f, 0);
        raycasterScript.HorizontalCollisions(ref facing);
        GameObject[] hits = null;
        bool anyHits = true;

        Debug.Log("movementControllerScript.facing = " + movementControllerScript.facing);
        if (movementControllerScript.facing > 0)
        {
            hits = raycasterScript.collisionState.rightHits;
            anyHits = raycasterScript.collisionState.anyRightHits();
            Debug.Log("anyHits = " + anyHits);
        }else if (movementControllerScript.facing < 0)
        {
            Debug.Log("facing.x = " + facing.x);
            hits = raycasterScript.collisionState.leftHits;
            anyHits = raycasterScript.collisionState.anyLeftHits();
        }

        if (anyHits)
        {
            Debug.Log("hits");
            movementControllerScript.velocity.y += .1f;
        }else
        {
            Debug.Log("no hits");
            movementControllerScript.velocity.y = 0;
            movementControllerScript.velocity.x = facing.x;
            hangingState = HangingState.NONE;
        }




    }

    void DetermineHangingState()
    {
        if (hangingState == HangingState.NONE)
        {
            thingImHangingOn = null;
            ShouldIGrab();
        }
        else if (hangingState == HangingState.DROPPING)
        {
            if (thingImHangingOn != null)
            {
                if (thingImHangingOn.GetComponent<MovementControllerScript>() != null)
                {
                    movementControllerScript.velocity += thingImHangingOn.GetComponent<MovementControllerScript>().velocity;
                }
            }
            thingImHangingOn = null;
            dropFrames++;
                if (dropFrames > 5)
                {
                    hangingState = HangingState.NONE;
                }
            }
        else if(hangingState == HangingState.HANGING)
        {
            movementControllerScript.velocity = new Vector2(0, 0);
        }
    }

    bool ShouldIGrab()
    {   if (movementControllerScript.velocity.y < 0)
        {
            Vector2 origin;

            if (movementControllerScript.facing == -1)
            {
                origin = raycasterScript.raycastOrigins.bottomLeft;
            }
            else
            {
                origin = raycasterScript.raycastOrigins.bottomRight;
            }

            origin += new Vector2(0, grabPointY);

            RaycastHit2D[] hits = Physics2D.RaycastAll(origin, new Vector2(movementControllerScript.facing, 0),
            raycasterScript.skinWidth + .01f, raycasterScript.collisionLayer);

            Debug.DrawRay(origin, new Vector2(movementControllerScript.facing, 0), Color.blue) ;

            RaycastHit2D grabPointHit = new RaycastHit2D();
            grabPointHit.distance = Mathf.Infinity;

            foreach (RaycastHit2D checkHit in hits)
            {
                if (!raycasterScript.gameObjectsIshouldIgnore.Contains(checkHit.transform.gameObject))
                {
                    if (checkHit.distance < grabPointHit.distance)
                    {
                        grabPointHit = checkHit;
                    }
                }
            }

            if (grabPointHit.transform != null)
            {

                origin += new Vector2(0, distanceAboveGrabPoint);

                hits = Physics2D.RaycastAll(origin, new Vector2(movementControllerScript.facing, 0),
                raycasterScript.skinWidth + .001f, raycasterScript.collisionLayer);
                Debug.DrawRay(origin, new Vector2(movementControllerScript.facing, 0), Color.yellow);

                RaycastHit2D hitAbove = new RaycastHit2D();
                hitAbove.distance = Mathf.Infinity;
                bool canIGrab = true;
                foreach (RaycastHit2D checkHit in hits)
                {
                    if (!raycasterScript.gameObjectsIshouldIgnore.Contains(checkHit.transform.gameObject))
                    {
                        Debug.Log("hit = " + checkHit.transform.gameObject);
                        canIGrab = false;
                        break;
                    }
                }

                if (canIGrab)
                {
                    thingImHangingOn = grabPointHit.transform.gameObject;
                    hangingState = HangingState.HANGING;
                    movementControllerScript.velocity.y = 0;
                    movementControllerScript.velocity.x = 0;
                    movementControllerScript.amountLeftToMove = new Vector2(0, 0);
                    return true;
                }
            }
        }
        hangingState = HangingState.NONE;
        return false;
    }

    public void drop()
    {
        hangingState = HangingState.DROPPING;
        dropFrames = 0;
    }


}
