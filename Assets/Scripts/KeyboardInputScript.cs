﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class KeyboardInputScript : MonoBehaviour {
    PlayerScript playerScript;

    int i = 0;

    public KeyCode leftKey = KeyCode.A;
    public KeyCode rightKey = KeyCode.D;
    public KeyCode upKey = KeyCode.W;
    public KeyCode downKey = KeyCode.S;
    public KeyCode pushKey = KeyCode.LeftShift;

	// Use this for initialization
	void Start () {
        playerScript = GetComponent<PlayerScript>();
	}
	
	// Update is called once per frame
	void LateUpdate () {
        i++;
		if (Input.GetKey(leftKey))
        {
            playerScript.leftPressed();
        }
        if (Input.GetKey(rightKey))
        {
            playerScript.rightPressed();
        }
        if (Input.GetKey(upKey))
        {
            playerScript.upPressed();
        }
        if (Input.GetKey(downKey))
        {
            playerScript.downPressed();
        }

        playerScript.pushKeyPressed = Input.GetKey(pushKey);
        if (i > 5)
        {
            playerScript.leftPressed();
//            playerScript.pushKeyPressed = true;

        }
    }
}